const eslint = require('gulp-eslint');
const gulp = require('gulp');
const gulpIf = require('gulp-if');
const jsdoc = require('gulp-jsdoc3');
const spawn = require('child_process').spawn;

const nodeProjectStandardDocFiles = ['README.md', 'index.js', './src/**/*.js'];
const emberProjectStandardDocFiles = ['README.md', 'app/**/*.js', 'config/**/*.js'];

const nodeProjectStandardLintFiles = ['*.js', 'src/**/*.js', 'test/**/*.js'];
const emberProjectStandardLintFiles = ['*.js', 'app/**/*.js', 'config/**/*.js', 'tests/**/*.js'];

/**
 * Generates jsdoc html files. If no config is specified, default values will be
 * pulled from node_modules/gulp-jsdoc3/src/jsdocConfig.json.
 *
 * @param {function} cb - Gulp callback
 * @param {Array} directoryList - The list of glob directory paths to include
 * @param {JsonFile} [config=undefined] - Jsdoc config file (optional)
 * @returns {undefined}
 */
function runDoc(cb, directoryList, config = undefined) {
    gulp.src(directoryList, { read: false })
        .pipe(jsdoc(config, cb));
}

/**
 * Used by eslint to determine if lint errors were automatically fixed or not.
 *
 * @private
 * @param {file} file - The .js file in question
 * @returns {boolean} - Whether or not lint errors were automatically fixed.
 */
function isFixed(file) {
    // Has ESLint fixed the file contents?
    return file.eslint != null && file.eslint.fixed;
}

/**
 * Runs eslint on a standard set of files and directories:
 *  - ./&#42;.js
 *  - src/&#42;&#42;/&#42;.js
 *  - test/&#42;&#42;/&#42;.js
 *
 * .eslintignore and .eslintrc.json are indeed respected when using gulp.
 *
 * @param {Array} directoryList - The list of glob directory paths to include
 * @param {boolean} [shouldFix=false] - Whether or not to automatically fix any
 *      lint errors that can normally be fixed via 'eslint --fix'.
 * @returns {any} Gulp result
 */
function runLint(directoryList, shouldFix = false) {
    // Because of how gulp.src works, it's far too slow to include a wildcard
    // for all js files and then exclude node_modules, ect. Instead, you should
    // include only the directories you want to lint. When using multiple globs,
    // make sure that you specify a base directory, or gulp.dest() will get
    // confused and likely output files to the wrong location. This is not very
    // well documented at all, and was a huge source of confusion.
    return gulp.src(directoryList, { base: './' })
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(eslint({ fix: shouldFix }))
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // if fixed, write the file to dest
        .pipe(gulpIf(isFixed, gulp.dest('./')))
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
}

/**
 * Runs mocha tests recursively on the directory specified. Mochawesome is used as
 * the reporter, and will generate html result files.
 *
 * @param {string} directory - The directory path to run mocha tests on (recursive).
 * @param {function} cb - Gulp callback
 * @returns {undefined}
 */
function runMochaTests(directory, cb) {
    // gulp-mocha does not support --reporter-options flag, so we'll call manually.
    // Use spawn, rather than exec here, as spawn streams output real time, whereas
    // exec buffers it. Aside from not getting real time results, exec can experience
    // buffer overflow errors with results greater than 200k. Note that {shell:true}
    // is an option required to fix a known (and not propery resolved for years) issue
    // on Windows systems only:
    // https://github.com/nodejs/node-v0.x-archive/issues/2318
    const proc = spawn('mocha', [
        directory,
        '--color',
        '--recursive',
        '--reporter mochawesome',
        '--reporter-options reportDir=test/generated-reports/unit,reportName=unit-tests-report,inlineAssets=true',
    ], { shell: true, stdio: 'inherit' });

    proc.on('error', (err) => {
        console.log(err);
        cb(err);
    });
    proc.on('exit', () => {
        cb();
    });
}

module.exports = {
    runDoc,
    runLint,
    runMochaTests,
    nodeProjectStandardDocFiles,
    nodeProjectStandardLintFiles,
    emberProjectStandardDocFiles,
    emberProjectStandardLintFiles,
};
